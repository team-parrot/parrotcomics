let microdata = require('microdata-node');
const axios = require('cachios');
const _ = require('lodash');
require('colors');

const XPathTools = require('../XpathTools.js');
const Blocks = require('./Commons.js');

class BedethequeSeriesExtractor {

    static matches(url) {
        return (/bedetheque.com\/serie/i.test(url));
    }

    static getNormalizedBlocks(url, sessionBag) {
        console.assert(BedethequeSeriesExtractor.matches(url));

        return axios
            .get(url)
            .then(response => microdata.toJson(response.data, {base: url}))
            .then(md => {
                console.log('Got raw µdata from '.cyan + url.yellow);
                return BedethequeSeriesExtractor.extractBlocksFromMicrodata(md, url, sessionBag);
            }).then(normalizedBlocks => {
                console.log('Normalized data from '.cyan + url.yellow);
                return normalizedBlocks;
            });
    }

    /**
     * Extracts blocks from microdata.
     * @param microdata
     * @param originURL
     * @param sessionBag
     * @returns {Promise<{series: Array, books: Array, persons: Array, others: Array}>}
     */
    static async extractBlocksFromMicrodata(microdata, originURL, sessionBag) {
        let normalized = Blocks.newNormalizedBlocks();
        let series = Blocks.newEmptySeries(originURL, 10);

        // List of all promises to wait for completion before this function returns
        let concurrentPromises = [];

        // Get the name of the series from the URL
        const seriesNameRegexResult = originURL.match(/www\.bedetheque\.com\/serie-9-BD-(.*?)(__10000)*\.html/i);
        if (seriesNameRegexResult && seriesNameRegexResult.length > 1) {
            series.seriesName = seriesNameRegexResult[1];
        }

        // Fetch the pretty name of the series using XPath on the page HTML
        concurrentPromises.push(XPathTools.getXpathResultFromURL(
            '//body//div[@class="bandeau-principal"]//h1//a/text()', originURL)
            .then(xpathResult => {
                series.seriesName = String(xpathResult[0]);
            })
            .catch(error => {
                console.log("Can't get pretty series name: ".red + series.seriesName.yellow);
                console.error(error);
            }));

        // For each item on bedetheque, ie for each book
        const personRegex = /<.*>/i;
        for (let item of microdata.items) {
            // console.log(JSON.stringify(item).magenta);

            let book = Blocks.newEmptyBook(originURL, 10);
            book.publishedDate = item.properties.datePublished[0];
            book.urlCover = item.properties.image[0];

            const cleanNameRegex = /\s\s[ ]{56}(.+)\s\s[ ]{56}\.[ ]{9}\s\s[ ]{56}(.*?)[ ]{48}/i;
            for (let name of item.properties.name) {
                let cleaned = cleanNameRegex.exec(name);
                if (cleaned && cleaned.length > 2) {
                    const cleanName = cleaned[2];
                    const cleanNumber = cleaned[1];
                    const cleanNumberedName = cleaned[1] + '. ' + cleaned[2];
                    Blocks.addTo(series, 'booksName', cleanNumberedName);
                    Blocks.saveTo(book, 'bookName', cleanName);
                    Blocks.saveTo(book, 'bookNumber', cleanNumber);
                } else {
                    const cleanName = name.trim();
                    Blocks.addTo(series, 'booksName', cleanName);
                    Blocks.saveTo(book, 'bookName', cleanName);
                }
            }

            // TODO: until we get a good bedetheque microdata extractor, [0] without a check
            Blocks.saveTo(book, 'isbn', item.properties.isbn[0]);
            Blocks.addTo(book, 'authors', item.properties.author, personRegex);
            Blocks.addTo(book, 'illustrators', item.properties.illustrator, personRegex);
            Blocks.addTo(book, 'editors', item.properties.publisher);
            Blocks.saveTo(book, 'numberOfPages', Number(item.properties.numberOfPages[0]));

            // Save book information
            normalized.books.push(book);

            // Add information to series
            Blocks.addToCounting(series, 'authors', item.properties.author, personRegex);
            Blocks.addToCounting(series, 'illustrators', item.properties.illustrator, personRegex);
            Blocks.addToCounting(series, 'editors', item.properties.publisher);

            // // Add information to persons
            // for (let authorName of item.properties.author) {
            //     if (!personRegex.test(authorName)) {
            //         normalized.persons.push({
            //             sources: [originURL],
            //             reliability: 10,
            //             name: authorName,
            //             type: "author"
            //         });
            //     }
            // }
            // for (let illustratorName of item.properties.illustrator) {
            //     if (!personRegex.test(illustratorName)) {
            //         normalized.persons.push({
            //             sources: [originURL],
            //             reliability: 10,
            //             name: illustratorName,
            //             type: "illustrator"
            //         });
            //     }
            // }
            // for (let editorName of item.properties.publisher) {
            //     if (!personRegex.test(editorName)) {
            //         normalized.persons.push({
            //             sources: [originURL],
            //             reliability: 10,
            //             name: editorName,
            //             type: "editor"
            //         });
            //     }
            // }
        }

        // Save series information
        normalized.series.push(series);

        // Put links to persons to the bag
        concurrentPromises.push(XPathTools.getXpathResultFromURL(
            '//ul[@class="infos"]//@href', originURL)
            .then(xpathResult => xpathResult.map(x => x.value))
            .then(urls => _.uniqWith(urls, _.isEqual))
            .then(urls => {
                console.error(urls);
                urls.forEach(url => {
                    sessionBag.push(url);
                });
            })
            .catch(error => {
                console.log("Can't get more from bedetheque: ".red);
                console.error(error);
            }));

        // // Get more information
        // concurrentPromises.push(BedethequeSeriesExtractor
        //     .getMorePlease(originURL, normalized, sessionBag));

        await Promise.all(concurrentPromises);
        return normalized;
    }
}

module.exports = BedethequeSeriesExtractor;
