const axios = require('axios');
const xpath = require('xpath');
const XMLDOMParser = require('xmldom').DOMParser;

/**
 * TODO
 */
class XpathTools {

    static async getXpathResultFromURL(xpathQuery, url) {
        return (await XpathTools.getMultipleXpathResultFromURL([xpathQuery], url))[0];
    }

    static getMultipleXpathResultFromURL(xpathQueries, url) {
        return axios
            .get(url, {
                responseType: "text/html",
                headers: {
                    "User-Agent": "Mozilla/4.0"
                }
            })
            .then((response) => {
                const doc = new XMLDOMParser({
                    locator: {},
                    errorHandler: {
                        warning: null,
                        error: () => {}
                    }
                }).parseFromString(response.data, "text/html; charset=utf-8");
                return xpathQueries.map(query => xpath.select(query, doc));
            });
    }
}

module.exports = XpathTools;
