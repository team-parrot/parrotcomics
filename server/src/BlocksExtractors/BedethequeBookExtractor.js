let microdata = require('microdata-node');
const axios = require('cachios');
require('colors');

const XPathTools = require('../XpathTools.js');
const Blocks = require('./Commons.js');

class BedethequeBookExtractor {

    static matches(url) {
        return (/bedetheque.com\/BD-/i.test(url));
    }

    static async getNormalizedBlocks(url, sessionBag) {
        console.assert(BedethequeBookExtractor.matches(url));

        let normalized = Blocks.newNormalizedBlocks();
        let book = Blocks.newEmptyBook(url, 10);
        let concurrentPromises = [];

        // Name, series' name, ULR
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                '//div[@class="bandeau-info album"]//h2/text()',
                '//div[@class="bandeau-info album"]//h1/a/text()',
                '//div[@class="bandeau-info album"]//h1/a/@href',
                '//span[@itemprop="isbn"]/text()',
                '//span[@itemprop="numberOfPages"]/text()',
                '//p[@id="p-serie"]//text()',
                '//img[@itemprop="image"]/@src',
            ], url)
            .then(xpathResults => {
                let bnr = /([\w]+)\s*,\.\s+(.*)/i.exec(String(xpathResults[0]).trim());
                if (bnr && bnr.length > 2) {
                    book.bookName = bnr[2];
                    book.bookNumber = bnr[1];
                } else {
                    book.bookName = String(xpathResults[0]).trim();
                }
                book.seriesName = String(xpathResults[1]).trim();
                book.isbn = String(xpathResults[3]);
                book.numberOfPages = Number(String(xpathResults[4]));
                book.synopsis = String(xpathResults[5]).trim();
                book.urlCover = /\w+="([^"]+)"/.exec(String(xpathResults[6]))[1];

                console.error(/\w+="([^"]+)"/.exec(String(xpathResults[2]))[1]);
                sessionBag.push(/\w+="([^"]+)"/.exec(String(xpathResults[2]))[1]);
            })
            .catch(error => {
                console.log("Can't get book from bedetheque.com.".red);
                console.error(error);
            }));

        await Promise.all(concurrentPromises);
        normalized.books.push(book);
        return normalized;
    }
}

module.exports = BedethequeBookExtractor;
