class Card {
    constructor(type, descriptor, value) {
        this.type = type;
        this.descriptor = descriptor;
        this.value = value;
    }
}

module.exports = Card;