import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {EnginePerson} from "../../../../services/engine/entities/engine.person";

const LOCALE_TYPES: object = {
  author: "Auteur",
  illustrator: "Dessinateur",
  editor: "Éditeur"
};

@Component({
  selector: 'card-personcontent',
  templateUrl: './personcontent.component.html',
  styleUrls: ['./personcontent.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PersonContentComponent implements OnInit {

  @Input() element: EnginePerson;

  constructor() { }

  ngOnInit() {
  }

  localisedType() {
    return LOCALE_TYPES[this.element.type];
  }

}
