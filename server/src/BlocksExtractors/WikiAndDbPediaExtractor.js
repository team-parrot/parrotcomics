const axios = require('cachios');
require('colors');

const Blocks = require('./Commons.js');

class WikiAndDbPediaExtractor {

    static matches(url) {
        return (
            /wikipedia\.org\/wiki\/(.+)/.test(url) ||
            /dbpedia\.org\/(.+)/.test(url)
        );
    }

    static getNormalizedBlocks(url, sessionBag) {
        console.assert(WikiAndDbPediaExtractor.matches(url));
        console.log("OMG, Wikipedia / DbPedia!".rainbow);

        const regexRes = /\/\/(.+)\.(wiki)?(db)?pedia\.org\/[^/]+\/(.+)/.exec(url)
        const page = regexRes[4];
        const lang = regexRes[1];
        const dbpediaUrl = "http://" + lang + ".dbpedia.org/data/" + encodeURI(page) + ".json";
        return axios
            .get(dbpediaUrl, {
                responseType: "application/json",
                headers: {"User-Agent": "Mozilla/4.0"}
            }).then(response => {
                console.log('Got JSON directly from DBPedia: '.green + dbpediaUrl.yellow);
                const ret = response.data["http://fr.dbpedia.org/resource/" + page];
                return (ret !== undefined) ? ret : {};
            }).then(md => {
                console.log('Got raw µdata from '.cyan + url.yellow);
                return WikiAndDbPediaExtractor.extractBlocksFromMicrodata(md, url, sessionBag);
            }).then(normalizedBlocks => {
                console.log('Normalized data from '.cyan + url.yellow);
                return normalizedBlocks;
            }).catch((error) => {
                console.log('Error when getting normalized blocks: '.red + String(error).red);
                console.error(error);
                return {};
            });
    }

    /**
     * Extracts blocks from microdata.
     * @param microdata
     * @param originURL
     * @returns {Promise<{series: Array, books: Array, persons: Array, others: Array}>}
     */
    static async extractBlocksFromMicrodata(microdata, originURL, sessionBag) {
        let normalized = Blocks.newNormalizedBlocks();

        const page = /\.org\/[^/]+\/(.+)/.exec(originURL)[1];

        //Try to find the type of data : book, author, or series
        let dataType;
        if (microdata["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"] !== undefined) {
            for (let type of microdata["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"]) {
                if (type.value === "http://schema.org/Person") {
                    dataType = "author";
                    break;
                }
                else if (type.value === "http://dbpedia.org/ontology/Comic") {
                    dataType = "book";
                    break;
                }
            }
        }
        if (dataType === undefined || dataType === "book") {
            if (microdata["http://purl.org/dc/terms/subject"] !== undefined) {
                for (let subject of microdata["http://purl.org/dc/terms/subject"]) {
                    if (/décès/i.test(subject.value)) {
                        dataType = "author";
                        break;
                    }
                    else if (/Série/i.test(subject.value)) {
                        dataType = "series";
                        break;
                    }
                }
            }
        }
        console.log(`OMG it's a ${dataType}!`.rainbow);

        if (dataType === "author") {
            let author = Blocks.newEmptyPerson(originURL, 5);

            for (let label of microdata["http://www.w3.org/2000/01/rdf-schema#label"]) {
                if (label.lang === 'fr') {
                    author.name = label.value;
                    break;
                }
            }
            if (microdata["http://fr.dbpedia.org/property/profession"] !== undefined) {
                author.job = microdata["http://fr.dbpedia.org/property/profession"][0].value;
            }
            else if (microdata["http://dbpedia.org/ontology/profession"] !== undefined) {
                author.job = microdata["http://dbpedia.org/ontology/profession"][0].value;
            }
            if (microdata["http://fr.dbpedia.org/property/dateDeNaissance"] !== undefined) {
                author.birthDate = microdata["http://fr.dbpedia.org/property/dateDeNaissance"][0].value;
            }
            if (microdata["http://fr.dbpedia.org/property/dateDeDécès"] !== undefined) {
                author.deathDate = microdata["http://fr.dbpedia.org/property/dateDeDécès"][0].value;
            }
            for (let abs of microdata["http://dbpedia.org/ontology/abstract"]) {
                if (abs.lang == "fr") {
                    author.bio = abs.value;
                }
            }
            if (microdata["http://fr.dbpedia.org/property/œuvresPrincipales"] !== undefined) {
                author.notableWorks = microdata["http://fr.dbpedia.org/property/œuvresPrincipales"][0].value;
            }
            else if (microdata["http://dbpedia.org/ontology/notableWork"] !== undefined) {
                author.notableWorks = [];
                for (let work of microdata["http://dbpedia.org/ontology/notableWork"]) {
                    author.notableWorks.push(work.value);
                }
            }
            /*	else if (microdata["http://fr.dbpedia.org/property/titre"] !== undefined) {
                    author.notableWorks = [];
                    for(let work of microdata["http://fr.dbpedia.org/property/titre"]) {
                        author.notableWorks.push(work.value);
                    }
                }*/
            normalized.persons.push(author);
        }
        else if (dataType === "book") {
            let book = Blocks.newEmptyBook(originURL, 5);

            book.bookName = page;
            if (microdata["http://fr.dbpedia.org/property/titre"] !== undefined) {
                book.bookName = microdata["http://fr.dbpedia.org/property/titre"][0].value;
            }
            if (microdata["http://www.w3.org/2000/01/rdf-schema#label"] !== undefined) {
                for (let label of microdata["http://www.w3.org/2000/01/rdf-schema#label"]) {
                    if (label.lang === "fr") {
                        book.bookName = label.value;
                    }
                }
            }
            if (microdata["http://fr.dbpedia.org/property/numéro"] !== undefined) {
                book.bookNumber = microdata["http://fr.dbpedia.org/property/numéro"][0].value;
            }
            if (microdata["http://fr.dbpedia.org/property/auteur"] !== undefined) {
                for (let auth of microdata["http://fr.dbpedia.org/property/auteur"]) {
                    book.authors.push(auth.value);
                }
            }
            else if (microdata["http://dbpedia.org/ontology/writer"] !== undefined) {
                for (let auth of microdata["http://dbpedia.org/ontology/writer"]) {
                    book.authors.push(auth.value);
                }
            }
            if (microdata["http://fr.dbpedia.org/property/dessin"] !== undefined) {
                for (let illus of microdata["http://fr.dbpedia.org/property/dessin"]) {
                    book.illustrators.push(illus.value);
                }
            }
            else if (microdata["http://dbpedia.org/ontology/illustrator"] !== undefined) {
                for (let illus of microdata["http://dbpedia.org/ontology/illustrator"]) {
                    book.illustrators.push(illus.value);
                }
            }
            if (microdata["http://fr.dbpedia.org/property/série"] !== undefined) {
                book.seriesName = microdata["http://fr.dbpedia.org/property/série"][0].value;
            }
            if (microdata["http://fr.dbpedia.org/property/nombreDePage"] !== undefined) {
                book.numberOfPages = microdata["http://fr.dbpedia.org/property/nombreDePages"][0].value;
            }
            if (microdata["http://dbpedia.org/ontology/isbn"] !== undefined) {
                book.isbn = microdata["http://dbpedia.org/ontology/isbn"][0].value;
            }
            if (microdata["http://dbpedia.org/ontology/abstract"]) {
                for (let abs of microdata["http://dbpedia.org/ontology/abstract"]) {
                    if (abs.lang === "fr") {
                        book.synopsis = abs.value;
                    }
                }
            }
            if (microdata["http://fr.dbpedia.org/property/éditeur"] !== undefined) {
                book.editor = microdata["http://fr.dbpedia.org/property/éditeur"][0].value;
            }
            if (microdata["http://fr.dbpedia.org/property/premièrePublication"] !== undefined) {
                book.publishedDate = microdata["http://fr.dbpedia.org/property/premièrePublication"][0].value;
            }
            normalized.books.push(book);
        }
        else if (dataType === "series") {
            let series = Blocks.newEmptySeries(originURL, 5);

            series.seriesName = page;
            if (microdata["http://www.w3.org/2000/01/rdf-schema#label"] !== undefined) {
                for (let label of microdata["http://www.w3.org/2000/01/rdf-schema#label"]) {
                    if (label.lang === "fr") {
                        series.seriesName = label.value;
                    }
                }
            }
            if (microdata["http://fr.dbpedia.org/property/auteur"] !== undefined) {
                for (let auth of microdata["http://fr.dbpedia.org/property/auteur"]) {
                    Blocks.addToCounting(series, 'authors', auth.value);
                }
            }
            if (microdata["http://fr.dbpedia.org/property/dessin"] !== undefined) {
                for (let illus of microdata["http://fr.dbpedia.org/property/dessin"]) {
                    Blocks.addToCounting(series, 'illustrators', illus.value);
                }
            }
            if (microdata["http://fr.dbpedia.org/property/premièrePublication"] !== undefined) {
                series.firstPublicationDate = microdata["http://fr.dbpedia.org/property/premièrePublication"][0].value;
            }
            else if (microdata["http://dbpedia.org/ontology/firstPublicationDate"] !== undefined) {
                series.firstPublicationDate = microdata["http://dbpedia.org/ontology/firstPublicationDate"][0].value;
            }
            if (microdata["http://purl.org/dc/elements/1.1/publisher"] !== undefined) {
                for (let pub of microdata["http://purl.org/dc/elements/1.1/publisher"]) {
                    Blocks.addToCounting(series, 'editors', pub.value);
                }
            }
            if (microdata["http://fr.dbpedia.org/property/nombreD'albums"] !== undefined){
                series.booksNumber = microdata["http://fr.dbpedia.org/property/nombreD'albums"][0].value;
            }
            normalized.series.push(series);
        }
        else {
            let other = Blocks.newEmptyOther(originURL, 5);
            let empty = true;

            if (microdata["http://www.w3.org/2000/01/rdf-schema#label"] !== undefined) {
                for (let label of microdata["http://www.w3.org/2000/01/rdf-schema#label"]) {
                    if (label.lang === "fr") {
                        other.value = label.value;
                        other.descriptor = "DBPedia data";
                        other.type = "Data";
                        empty = false;
                    }
                }
            }
            if (!empty) normalized.others.push(other);
        }

        // Fill the bag
        if (microdata["http://purl.org/dc/terms/subject"] !== undefined) {
            for (let item of microdata["http://purl.org/dc/terms/subject"]){
                sessionBag.push(item.value);
                console.log("Added to sessionBag: ".magenta + item.value.yellow);
            }
        }

        return normalized;
    }
}

module.exports = WikiAndDbPediaExtractor;
