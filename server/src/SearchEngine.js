const axios = require('axios');
const xpath = require('xpath');
const XMLDOMParser = require('xmldom').DOMParser;
const _ = require('lodash');
const keygen = require("keygenerator");
require('colors');

const BlocksExtractor = require('./BlocksExtractor.js');
const XPathTools = require('./XpathTools.js');

/**
 * TODO
 */
class SearchEngine {

    constructor() {
        this.sessionsBags = {};
        this.sessionsSeenBags = {};
    }

    /**
     * Handle the query.
     * Required parameters: query or session_id
     *
     * @param query
     * @returns {Promise}
     */
    async handle(query) {
        // 0. Ow, so much work to accomplish...
        let data = {};

        // 1. If query contains a query string, fill the bag
        if ('query' in query) {
            console.log('Received query: '.cyan + query.query);

            // Get a new session ID if not one already
            if (!('session_id' in query)) {
                data.session_id = this.getNewSessionId();
                this.sessionsSeenBags[data.session_id] = [];
            } else {
                data.session_id = query.session_id;
                if (this.sessionsSeenBags[data.session_id] === undefined) {
                    this.sessionsSeenBags[data.session_id] = [];
                }
            }

            // Query a search engine
            // query = SearchEngine.prepareQuery(query);
            try {
                let newLinks = [];
                this.sessionsBags[data.session_id] = (await SearchEngine
                    .getSearchEngineResults(query.query, newLinks)).concat(newLinks);

            } catch (error) {
                console.log('Error when querying the search engine:'.red);
                console.error(error);
                throw "Can't access the search engine.";
            }
        }  else if (!('session_id' in query)) {
            throw "You have to specify at least a query or a session_id.";
        } else {
            data.session_id = query.session_id;
            if (!(data.session_id in this.sessionsBags)) {
                throw "Invalid session_id.";
            }
        }

        // 2. Tries to get normalized blocks objects from each URL in this session's bag
        let normalizedBlocksList = [];
        try {
            // this.sessionsBags[data.session_id] = _.shuffle(this.sessionsBags[data.session_id]);
            let blocksPromisesList = [];
            while (blocksPromisesList.length === 0 && this.sessionsBags[data.session_id].length > 0) {
                blocksPromisesList = blocksPromisesList.concat(this.sessionsBags[data.session_id]
                    .splice(0, 10)
                    .map(url => {
                            this.sessionsSeenBags[data.session_id].push(url);

                            console.log("Extracting from: ".magenta + url.yellow);
                            return BlocksExtractor
                                .extractBlocks(url, this.sessionsBags[data.session_id])
                                .catch((error) => {
                                    console.log('Error when getting blocks: '.red + String(error).red);
                                    if (!/[4-5]0\d/.test(String(error))) {
                                        console.error(error);
                                    }
                                })
                        }
                    ));
            }
            for (let xs of await Promise.all(blocksPromisesList)) {
                normalizedBlocksList.push.apply(normalizedBlocksList, xs);
            }
            console.log("New bag size: ".cyan + String(this.sessionsBags[data.session_id].length).yellow);
        } catch (error) {
            console.log('Error when getting blocks:'.red);
            console.error(error);
            throw "Can't get blocks.";
        }

        // 3. Merge all the normalized blocks together
        try {
            data.blocks = SearchEngine.aggregate(normalizedBlocksList);
        } catch (error) {
            console.log('Error when merging blocks:'.red);
            console.error(error);
            throw "Can't merge blocks.";
        }

        // 4. Remove already seen URLs from sessionsBag
        this.sessionsBags[data.session_id] = _.difference(
            this.sessionsBags[data.session_id], this.sessionsSeenBags[data.session_id]);

        // 5. Yay!
        return data;
    }

    /**
     * Return a new unused session ID.
     */
    getNewSessionId() {
        let id = undefined;
        do {
            id = keygen.session_id();
        } while (id in this.sessionsBags);
        console.log('New session ID: '.cyan + id.yellow);
        return id;
    }

    /**
     * Prepare the input query.
     * @param query
     * @returns {String}
     */
    static prepareQuery(query) {
        query.query += "+bande+dessinée";
        return query;
    }

    /**
     *
     * @param query
     */
    static async getSearchEngineResults(query, sessionBag) {
        async function getGoogleResults(queryString, start = 0, addToEnd = '+bande+dessinée') {
            // 1. Query a search engine
            const queryUrl = 'https://www.google.fr/search?q=' + query + addToEnd + '&start=' + start;
            console.log('Querying: '.cyan + queryUrl.yellow);
            const response = await axios
                .get(queryUrl, {
                    responseType: "text",
                    headers: {
                        "User-Agent": "Mozilla/4.0"
                    }
                });

            // 2. Get the result URL from the search engine's response using XPath
            const xpathResult = xpath
                .select("//body//p//a/@href", new XMLDOMParser({
                        locator: {},
                        errorHandler: {
                            warning: null,
                            error: null
                        }
                    }).parseFromString(response.data, "text/html; charset=utf-8")
                );

        // 3. Get URL from Xpath results
        let promisesUrls = [];
        for (let node of xpathResult) {
            let prettyNodeRegexRes = /href="\/url\?q=(.*)&amp;sa.*/gi.exec(String(node));
            if (prettyNodeRegexRes && prettyNodeRegexRes.length > 1) {
                let prettyURL = decodeURI(prettyNodeRegexRes[1]);
                promisesUrls.push(SearchEngine.enhanceUrl(prettyURL, sessionBag));
            }
        }
        let urls = await Promise.all(promisesUrls);

            // 4. Remove duplicates, log and return
            urls = _.uniqWith(urls, _.isEqual);
            return urls;
        }
        let urls = [];
        for (let i = 0; i < 2; ++i) {
            urls = _.uniqWith(urls.concat(await getGoogleResults(query, i * 10)), _.isEqual);
        }
        urls = _.uniqWith(urls.concat(await getGoogleResults(query, 0, '+wikipedia')), _.isEqual);
        console.log('Results: '.cyan + JSON.stringify(urls).yellow);
        return urls;
    }

    /**
     * Enhance known URL.
     * @param url
     */
    static async enhanceUrl(url, sessionBag) {
        // Redirect from BD to series
        if (/bedetheque.com\/BD-/i.test(url)) {
            try {
                // Get the series URL
                let xpathResult = await XPathTools.getXpathResultFromURL(
                    '//body//div[@class="bandeau-menu"]//li[last()]/a/@href',
                    url
                );
                if (xpathResult && xpathResult.length !== undefined) {
                    if (xpathResult.length === 0) {
                        console.log("bedeteque.com: Can't find the link to the series.".red);
                    }
                    else if (xpathResult.length > 1) {
                        console.log("bedeteque.com: Has more than one link to the series.".red);
                    }
                    else {
                        let seriesUrl = String(xpathResult[0]).match(/href="(.*)"/i)[1];
                        const esrr = /bedetheque.com\/serie.*?(__10000)?\.html$/i.exec(url);
                        if (esrr && esrr.length === 2) {
                            seriesUrl = seriesUrl.replace(/\.html/i, '__10000.html');
                        }
                        sessionBag.push(seriesUrl);
                        console.log(
                            "bedeteque.com: Adding the series to the bag: ".green +
                            seriesUrl.yellow
                        );
                    }
                }
            } catch (error) {
                console.log('Error when grabbing series link:'.red);
                console.error(error);
            }
        }

        // Redirect to all books
        const expandSeriesRegexRes = /bedetheque.com\/serie.*?(__10000)?\.html$/i.exec(url);
        if (expandSeriesRegexRes && expandSeriesRegexRes.length === 2) {
            url = url.replace(/\.html/i, '__10000.html');
            console.log(
                "bedeteque.com: Redirecting to the full series ".green +
                url.yellow
            );
        }

        return url;
    }

    /**
     * Aggregate / merge a list of blocks objects into a unique richer normalized block object.
     * @param nmdList
     * @returns {*}
     */
    static aggregate(nmdList) {
        if (nmdList.length === 0) return {};

        // TODO: Sort nmdList by increasing reliability

        /**
         * Merge what inside where: where = where + what.
         *
         * For each attribute of what:
         *   - If where doesn't possess this attribute, simply add it.
         *   - Otherwise:
         *     - If this attribute is a an array, merge the two arrays.
         *     - If it's not an array, update where's attribute only if what is more reliable.
         *
         * @param where
         * @param what
         */
        function merge(where, what) {
            // console.error('Duplicates: ', a, b);
            for (let key in what) if (what.hasOwnProperty(key)) {
                if (!(key in what) || where[key] === undefined || where[key].length === 0) {
                    where[key] = what[key];
                } else {
                    if (Array.isArray(where[key])) {
                        if (!Array.isArray(what[key])) {
                            console.log("ERROR: duplicates don't have the same structures.");
                            console.error('Duplicates: ', where, what);
                            continue;
                        }
                        where[key] = _.uniqWith(where[key].concat(what[key]), _.isEqual);
                    } else {
                        if (Array.isArray(what[key])) {
                            console.log("ERROR: duplicates don't have the same structures.");
                            console.error('Duplicates: ', where, what);
                            continue;
                        }
                        if (where.reliability < what.reliability) {
                            where[key] = what[key];
                        }
                    }
                }
            }
        }

        /**
         * Add a what to where, merging if an object with the same primaryKey already exists in where.
         * @param what: what to add
         * @param where: where to add in
         * @param primaryKeys: keys with which the unicity is checked, more prioritary first
         * @param uniquenessMap: a different map for each type (books, series, persons,...).
         *   Of type: `Object.create(null);`
         */
        function mergeAdd(what, where, primaryKeys, uniquenessMap) {
            if (!Array.isArray(primaryKeys)) {
                primaryKeys = [primaryKeys];
            }
            const hs = primaryKeys.filter(pk => pk in what).map(pk => String(what[pk]).toLowerCase());
            let merged = false;
            for (let h of hs) {
                if (uniquenessMap[h] !== undefined) {
                    merge(uniquenessMap[h], what);
                    merged = true;
                    const new_hs = primaryKeys
                        .filter(pk => pk in what)
                        .map(pk => String(what[pk]).toLowerCase())
                        .filter(h => !(h in hs));
                    for (let new_h of new_hs) {
                        uniquenessMap[new_h] = uniquenessMap[h];
                    }
                    break;
                }
            }
            if (!merged) {
                where.push(what);
                for (let h of hs) {
                    uniquenessMap[h] = what;
                }
            }
        }

        let aggregate = {
            series: [],
            books: [],
            persons: [],
            others: []
        };
        // https://stackoverflow.com/questions/25297332/javascript-map-variable#25297359
        let seriesMap = Object.create(null);
        let booksMap = Object.create(null);
        let personsMap = Object.create(null);

        // For each normalized blocks object
        for (let nmd of nmdList) {
            // Series
            for (let series of nmd.series) {
                mergeAdd(series, aggregate.series, 'seriesName', seriesMap);
            }
            // Books
            for (let book of nmd.books) {
                mergeAdd(book, aggregate.books, ['isbn', 'bookName'], booksMap);
            }
            // Persons
            for (let person of nmd.persons) {
                mergeAdd(person, aggregate.persons, 'name', personsMap);
            }
            // Others
            for (let other of nmd.others) {
                // TODO: Aggregate others
                aggregate.others.push(other);
            }
        }

        return aggregate;
    }
}

module.exports = SearchEngine;
