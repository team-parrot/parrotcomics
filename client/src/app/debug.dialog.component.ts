
import {MAT_DIALOG_DATA} from "@angular/material";
import {Component, Inject} from "@angular/core";

@Component({
  selector: 'debug-card-dialog',
  template: `
    <h2 mat-dialog-title>Debug Modal</h2>
    <mat-dialog-content><pre>{{data.object | json}}</pre></mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button mat-dialog-close><mat-icon>close</mat-icon> Fermer</button>
    </mat-dialog-actions>
  `
})
export class DebugDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
