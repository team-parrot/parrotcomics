const ExtractorInterface = require('./AbstractExtractor');

const CARD_MODELS = {
    seriesName: {multiple: false, type: "shortText", descriptor: "seriesName"},
    booksName: {multiple: true, type: "shortText", descriptor: "bookName"},
    authors: {multiple: true, type: "person", descriptor: "author"},
    illustrators: {multiple: true, type: "person", descriptor: "drawer"},
    firstPublicationDate: {multiple: false, type: "date", descriptor: "published"},
    editors: {multiple: true, type: "shortText", descriptor: "editor"},
    urlCouv: {multiple: false, type: "image", descriptor: "cover"},
};

class SeriesExtractor extends ExtractorInterface {

    constructor() {
        super(CARD_MODELS);
    }

}

module.exports = SeriesExtractor;