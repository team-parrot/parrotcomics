import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

import _ from 'lodash';

import {EngineResponse} from './entities/engine.response.interface';
import {EngineServiceInterface} from "./engine.service.interface";
import {EngineElementDefault} from "./entities/engine.element.default";
import {EngineBook} from "./entities/engine.book";
import {EngineSerie} from "./entities/engine.serie";
import {EnginePerson} from "./entities/engine.person";
import {EngineElementInterface} from "./entities/engine.element.interface";

const SORT_SCORE_RELIABILITY = 0.7;
const SORT_SCORE_SOURCES_LEN = 0.3;

const ELEMENT_CLASSES = {
  series: EngineSerie,
  persons: EnginePerson,
  books: EngineBook,
  others: EngineElementDefault
};

const ENABLE_CARD_SHUFFLE: boolean = false;

@Injectable()
export class EngineService implements EngineServiceInterface {

  public endpoint: string;
  private sessionId: string;

  constructor(private http: HttpClient) {

  }

  search(query: string, followSession: boolean = false): Promise<Array<EngineElementInterface>> {
    if (false === followSession) this.sessionId = undefined;

    let params = this.buildParams(query, followSession);

    return new Promise((resolve, reject) => {

      if ('undefined' === typeof this.endpoint || 0 === this.endpoint.length) {
         reject({message: "Endpoint not defined."});
         return;
      }

      this.http.get<EngineResponse>(this.endpoint, {params})
        .subscribe(response => {
          if (true === response.success) {

            console.info('[EngineService] Successfully received a response from the server.');
            console.info('[EngineService] Current session id: '+response.session_id);
            console.debug(response);

            this.sessionId = response.session_id;

            resolve(EngineService.parseElements(response.blocks));
            return;
          }

          reject(response.error)
        },
          err => {
            console.error('[EngineService] Failed to retrieve results from Server.', err);
            reject(err)
          });
    });
  }

  buildParams(query: string, followSession: boolean): {query ?: string, session_id ?: string} {
    return (true === followSession) ? {session_id: this.sessionId} : {query};
  }

  static parseElements(
    elementsObj: {
      series: Array<EngineSerie>,
      books: Array<EngineBook>,
      persons: Array<EnginePerson>,
      others: Array<EngineElementDefault>
    }
  ): Array<EngineElementInterface> {

    let elements = [];

    // Let's """cast""" the object into Engine entities.
    for (let key in elementsObj) {
      for (let element of elementsObj[key]) {
        let object = new ELEMENT_CLASSES[key]();
        Object.assign(object, element);
        elements.push(object);
      }
    }

    console.info('[EngineService] Response elements casted into engine entities.');

    return (ENABLE_CARD_SHUFFLE) ? _.shuffle(elements) : elements;
  }

}
