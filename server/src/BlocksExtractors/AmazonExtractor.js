const axios = require('cachios');
const entities = require('html-entities').AllHtmlEntities;
require('colors');

const XPathTools = require('../XpathTools.js');
const Blocks = require('./Commons.js');

class AmazonExtractor {

    static matches(url) {
        return (/\.amazon\.fr/i.test(url));
    }

    static async getNormalizedBlocks(url, sessionBag) {
        console.assert(AmazonExtractor.matches(url));


        let normalized = Blocks.newNormalizedBlocks();
        let concurrentPromises = [];

        // One "concurrent promise" per block
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                // Here, every XPath query used for this block
                '//*[@id="productTitle"]/text()',
                '//*[@id="buyNewSection"]//span[contains(@class,"a-color-price")]/text()',
            ], url)
            .then(xpathResults => {
                let prix = String(xpathResults[1]);
                if (/\d/.test(prix)) {
                    normalized.others.push({
                        sources: [url],
                        reliability: 1,
                        type: "price",
                        descriptor: entities.decode(entities.decode(String(xpathResults[0]).trim())),
                        value: String(xpathResults[1])
                    });
                }
                // No return, just modify directly normalized
            })
            .catch(error => {
                console.log("Can't get price from Amazon.fr.".red);
                // console.error(error);
            }));

        await Promise.all(concurrentPromises);
        return normalized;
    }
}

module.exports = AmazonExtractor;