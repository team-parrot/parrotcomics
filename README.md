# ParrotComics

ParrotComics est un moteur de recherche enrichi de bande dessinées. IL récupère et aggrège les données issues de divers fournisseurs tels que DBPedia (via Sparql), Google (déréférencement) et traitement des microdata.

Ce projet est réalisé dans le cadre des cours de Web Sémantique de l'INSA Lyon - Département informatique, sous licence GNU GPL v3.

Technologies utilisées:
* Client : Angular 5.0 / Typescript
* Serveur : Node.JS v8.9.0 / ECMAScript 6

## Installation

Première chose à faire avant tout : installer les dépendances Node.js:
1. Installer Node.js version 8.9.0 LTS
2. Installer le CLI Angular : `npm install -g @angular/cli` (permet d'utiliser la commande `ng`)
3. Aller dans les dossiers `client` et `server` et taper la commande : `npm install`

## Client : Principes et fonctionnement

Le client repose sur l'utilisation de requêtes ajax couplées à Angular et Angular Material pour la mise en forme des données.

Pour lancer le client, nous recommandons l'utilisation d'angular-cli et donc de lancer le client via la commande : `ng serve`

## Serveur : Principes et fonctionnement

pour lancer le serveur, depuis le dossier `./server` : `node index.js`

Le serveur contient un point d'entrée unique : `/search`, qui prend plusieurs paramètres:
* `query*` : Termes de la recherche. Obligatoire lorsque l'on cherche les premiers éléments.
* `session_id*` : ID de session dans laquelle sont contenus les résultats de recherche. Il faut le fournir obligatoirement à la place de `query` lorsqu'on veut plus de résultats.
* `limit*` : Nombre maximum de résultats de recherche à envoyer au client.
* `offset` : Exclut les X premiers éléments de recherche si offset = X.

*Pour le moment, le point d'entrée permet uniquement d'envoyer une requête sparql en query et de retourner la réponse.*