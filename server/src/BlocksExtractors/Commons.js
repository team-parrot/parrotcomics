function addSingleTo(where, key, value, filterRegex = undefined) {
    if (Array.isArray(value)) {
        addTo(where, key, value, filterRegex);
    }
    else if (filterRegex === undefined || !filterRegex.test(value)) {
        if (!Array.isArray(where[key])) {
            console.log('CRITICAL: '.bold.red + `where.${key} must be an array.`.red);
        }
        where[key].push(value);
    }
}

function addTo(where, key, values, filterRegex = undefined) {
    if (Array.isArray(values)) {
        for (let x of values) {
            addSingleTo(where, key, x, filterRegex);
        }
    } else {
        addSingleTo(where, key, values, filterRegex);
    }
}

function addSingleToCounting(where, key, value, filterRegex = undefined) {
    if (filterRegex === undefined || !filterRegex.test(value)) {
        if (where[key] === undefined) {
            console.log('CRITICAL: '.bold.red + 'trying to add a value to as unadded series property.'.red);
        }
        if (!where[key].hasOwnProperty(value)) where[key][value] = 0;
        where[key][value]++;
    }
}

function addToCounting(where, key, values, filterRegex = undefined) {
    if (Array.isArray(values)) {
        for (let x of values) {
            addSingleToCounting(where, key, x, filterRegex);
        }
    } else {
        addSingleToCounting(where, key, values, filterRegex);
    }
}

function saveTo(where, key, value, filterRegex = undefined) {
    if (filterRegex === undefined || !filterRegex.test(value)) {
        if (where[key] !== undefined) {
            console.log(`where.${key} was already defined.`.red);
        } else {
            where[key] = value;
        }
    }
}

function newNormalizedBlocks() {
    return {
        series: [],
        books: [],
        persons: [],
        others: []
    };
}

function newEmptyBook(originUrl, reliability) {
    return {
        sources: [originUrl],
        reliability: reliability,
        bookName: undefined,
        bookNumber: undefined,
        authors: [],
        illustrators: [],
        editors: []
    };
}

function newEmptySeries(originUrl, reliability) {
    return {
        sources: [originUrl],
        reliability: reliability,
        seriesName: undefined,
        authors: {},
        illustrators: {},
        editors: {},
        booksName: []
    };
}

function newEmptyPerson(originUrl, reliability) {
    return {
        sources: [originUrl],
        reliability: reliability,
        name: undefined
    };
}

function newEmptyOther(originUrl, reliability) {
    return {
        sources: [originUrl],
        reliability: reliability,
        label: undefined
    };
}

module.exports = {
    addTo: addTo,
    addToCounting: addToCounting,
    saveTo: saveTo,
    newNormalizedBlocks: newNormalizedBlocks,
    newEmptyBook: newEmptyBook,
    newEmptySeries: newEmptySeries,
    newEmptyPerson: newEmptyPerson,
    newEmptyOther: newEmptyOther,
};