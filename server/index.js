const App = require('./src/App.js');

// Override default console.log
const oldConsoleLog = console.log;

console.log = (message) => {
	oldConsoleLog('['+(new Date).toTimeString()+' - ParrotComics] '+message);
};

const app = new App();
app.init();