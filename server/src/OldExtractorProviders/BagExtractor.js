const AbstractExtractor = require('./AbstractExtractor');

class BagExtractor extends AbstractExtractor {

    constructor() {
        super({});
    }


    extract(aggregateArray) {
        console.log("Bag Extractor does absolutely nothing... Yoooohooo !".rainbow);

        return super.extract(aggregateArray);
    }
}

module.exports = BagExtractor;