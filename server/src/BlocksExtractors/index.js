module.exports = [
    require('./FnacExtractor.js'),
    require('./WikiAndDbPediaExtractor.js'),
    require('./BedethequeSeriesExtractor.js'),
    require('./BedethequeAuthorsExtractor.js'),
    require('./BedethequeBookExtractor.js'),
    require('./CDiscountExtractor.js'),
    require('./AmazonExtractor.js'),
];