class AbstractExtractor {

    constructor(cardModel = null) {
        this.cardModel = cardModel;
    }

    extract(aggregateArray) {
        let booksCards = [];
        console.error('begin extract ', aggregateArray);
        for (let subAggregate of aggregateArray) {
            console.error('Welcome in AbstractExtractor loop', subAggregate);
            for (let data in subAggregate) {
                console.error('Data traitee : ', data);
                let cardModel = this.cardModel[data];

                if ('undefined' === typeof cardModel) {
                    console.log(`CardExtractor - ${data} is not defined in card models. Skipping.`.red);
                    continue;
                }

                if (true === cardModel.multiple) {
                    console.error('subAggregate key : ', data);
                    console.error("Value of dataArray : ", subAggregate[data]);
                    for (let element in subAggregate[data]) {

                        let card = {};

                        card.type = cardModel.type;
                        card.descriptor = cardModel.descriptor;
                        card.value = element;

                        booksCards.push(card);
                    }
                } else {
                    let card = {};

                    card.type = cardModel.type;
                    card.descriptor = cardModel.descriptor;
                    card.value = subAggregate[data];

                    booksCards.push(card);
                }
            }
        }
        return booksCards;
    }

}

module.exports = AbstractExtractor;