import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {EngineBook} from "../../../../services/engine/entities/engine.book";

@Component({
  selector: 'card-bookcontent',
  templateUrl: './bookcontent.component.html',
  styleUrls: ['./bookcontent.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookContentComponent implements OnInit {

  @Input() element: EngineBook;

  constructor() { }

  ngOnInit() {
  }

}
