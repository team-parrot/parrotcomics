const PARROTS = [
  "angelparrot",
  "beretparrot",
  "birthdaypartyparrot",
  "christmasparrot",
  "congaparrot",
  "dealwithitparrot",
  "discoparrot",
  "donutparrot",
  "gentlemanparrot",
  "jediparrot",
  "middleparrot",
  "parrot",
  "popcornparrot",
  "prideparrot",
  "pumpkinparrot",
  "revolutionparrot",
  "rightparrot",
  "sadparrot",
  "scienceparrot",
  "shuffleparrot",
  "sushiparrot",
  "wendyparrot",
];

/**
 * A simple parrot asset provider for homepage.
 */
export class ParrotService {

  private parrotList: Array<String> = PARROTS;

  constructor() {}

  getParrot(): String {
    let index = Math.floor(Math.random() * this.parrotList.length);

    return this.parrotList[index];
  }

}
