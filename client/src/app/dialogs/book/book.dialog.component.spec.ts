import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Book.DialogComponent } from './book.dialog.component';

describe('Book.DialogComponent', () => {
  let component: Book.DialogComponent;
  let fixture: ComponentFixture<Book.DialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Book.DialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Book.DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
