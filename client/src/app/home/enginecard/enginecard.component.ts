import {Component, EventEmitter, Inject, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {animate, style, transition, trigger} from "@angular/animations";
import {EngineElementInterface} from "../../../services/engine/entities/engine.element.interface";
import {EngineSerie} from "../../../services/engine/entities/engine.serie";
import {EngineBook} from "../../../services/engine/entities/engine.book";
import {EnginePerson} from "../../../services/engine/entities/engine.person";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material";

import {DebugDialogComponent} from "../../debug.dialog.component";
import {SerieDialogComponent} from "../../dialogs/serie/serie.dialog.component";
import {BookDialogComponent} from "../../dialogs/book/book.dialog.component";
import {PersonDialogComponent} from "../../dialogs/person/person.dialog.component";
import {LinksDialogComponent} from "../../links.dialog.component";

const TYPES_DIALOG = {
  serie: SerieDialogComponent,
  book: BookDialogComponent,
  person: PersonDialogComponent
};

@Component({
  selector: 'app-enginecard',
  templateUrl: './enginecard.component.html',
  styleUrls: ['./enginecard.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('easeIn', [
      transition(':enter', [
        style({opacity: 0, transform: "translateY(90px)"}),
        animate('0.4s ease-out', style({opacity: 1, transform: "translateY(0)"}))
      ]),
      transition(':leave', [
        style({opacity: 1, transform: "translateX(0)"}),
        animate('0.4s ease-in', style({opacity: 0, transform: "translateX(-90px)"}))
      ])
    ])
  ]
})
export class EnginecardComponent implements OnInit {

  @Input() id: number;
  @Input() page: number;

  @Input() engineElement: EngineElementInterface;

  @Output() ignored = new EventEmitter<{id: number, page: number}>();

  constructor(private dialog: MatDialog) { }

  openDialog() {
    let contentType = this.checkContentType();

    this.dialog.open(TYPES_DIALOG[contentType], {
      data: {element: this.engineElement}
    })
  }

  debug() {
    this.dialog.open(DebugDialogComponent, {
      data: {object: this.engineElement},
      width: '60%',
    })
  }

  checkContentType(): string {
    if(this.engineElement instanceof EngineSerie) {
      return 'serie';
    } else if (this.engineElement instanceof EngineBook) {
      return 'book';
    } else if (this.engineElement instanceof EnginePerson) {
      return 'person';
    } else {
      return 'default';
    }
  }

  openSources() {
    this.dialog.open(LinksDialogComponent, {
      data: {links: this.engineElement.sources}
    })
  }

  ngOnInit() {
  }

}
