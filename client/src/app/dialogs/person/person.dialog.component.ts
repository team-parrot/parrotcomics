import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {EnginePerson} from "../../../services/engine/entities/engine.person";
import {MAT_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'app-person.dialog',
  templateUrl: './person.dialog.component.html',
  styleUrls: ['./person.dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PersonDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {element: EnginePerson}) { }

  ngOnInit() {
  }

}
