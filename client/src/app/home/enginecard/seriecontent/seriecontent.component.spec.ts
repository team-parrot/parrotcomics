import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerieContentComponent } from './seriecontent.component';

describe('SerieContentComponent', () => {
  let component: SerieContentComponent;
  let fixture: ComponentFixture<SerieContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerieContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerieContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
