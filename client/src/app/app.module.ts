import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
  MatButtonModule,
  MatCardModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule,
  MatProgressSpinnerModule, MatSnackBarModule,
  MatToolbarModule,
  MatExpansionModule, MatDialogModule,
} from "@angular/material";


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ParrotService } from "../services/parrot.service";
import { EngineMocker } from "../services/engine/engine.service.mocker";
import { EngineService } from "../services/engine/engine.service";
import { EngineElementMocker } from "../services/engine/entities/engine.element.mocker";
import { EnginecardComponent } from './home/enginecard/enginecard.component';
import {HttpClientModule} from "@angular/common/http";
import { SerieContentComponent } from './home/enginecard/seriecontent/seriecontent.component';
import { BookContentComponent } from './home/enginecard/bookcontent/bookcontent.component';
import { PersonContentComponent } from './home/enginecard/personcontent/personcontent.component';
import { DefaultContentComponent } from './home/enginecard/defaultcontent/defaultcontent.component';
import {DebugDialogComponent} from "./debug.dialog.component";
import { ExpanderComponent } from './expander/expander.component';
import {LinksDialogComponent} from "./links.dialog.component";
import { SerieDialogComponent } from './dialogs/serie/serie.dialog.component';
import { BookDialogComponent } from './dialogs/book/book.dialog.component';
import { PersonDialogComponent } from './dialogs/person/person.dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EnginecardComponent,
    SerieContentComponent,
    BookContentComponent,
    PersonContentComponent,
    DefaultContentComponent,
    ExpanderComponent,
    DebugDialogComponent,
    LinksDialogComponent,
    SerieDialogComponent,
    BookDialogComponent,
    PersonDialogComponent,
  ],
  entryComponents: [
    DebugDialogComponent,
    LinksDialogComponent,
    SerieDialogComponent,
    BookDialogComponent,
    PersonDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatCardModule,
    MatGridListModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDialogModule,
    InfiniteScrollModule,
  ],
  providers: [
    ParrotService,
    EngineElementMocker,
    {provide: EngineService, useClass: EngineService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
