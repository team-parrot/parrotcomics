import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Person.DialogComponent } from './person.dialog.component';

describe('Person.DialogComponent', () => {
  let component: Person.DialogComponent;
  let fixture: ComponentFixture<Person.DialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Person.DialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Person.DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
