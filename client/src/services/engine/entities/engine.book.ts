import {EngineElementInterface} from "./engine.element.interface";
import {NameNormalizer} from "../utils/name.normalizer";

export class EngineBook implements EngineElementInterface {

  public sources: Array<string>;
  public reliability: number;
  public bookName: string;
  public bookNumber: string;
  public isbn: string;
  public publishedDate: string;
  public urlCover: string;
  public numberOfPages: number;
  public seriesName: string;
  public seriesNo: string;
  public seriesTotal: string;
  public synopsis: string;
  private _authors: Array<string>;
  private _illustrators: Array<string>;
  private _editors: Array<string>;

  get editors(): Array<string> {
    return this._editors;
  }

  set editors(value: Array<string>) {
    this._editors = NameNormalizer.normalizeArray(value);
  }
  get illustrators(): Array<string> {
    return this._illustrators;
  }

  set illustrators(value: Array<string>) {
    this._illustrators = NameNormalizer.normalizeArray(value);
  }
  get authors(): Array<string> {
    return this._authors;
  }

  set authors(value: Array<string>) {
    this._authors = NameNormalizer.normalizeArray(value);
  }

}
