
const PROVIDERS = require('./OldExtractorProviders');

class AggregateExtractor {

    constructor() {
        this.providers = {};

        for (let provider in PROVIDERS) if (PROVIDERS.hasOwnProperty(provider)) {
            this.providers[provider] = new PROVIDERS[provider]();
        }
    }

    extract(aggregate) {
        let cards = [];
        // console.error(aggregate);
        for (let key in aggregate) if (aggregate.hasOwnProperty(key)) {
            try {
                // The link between the sub-aggregate and the extractor is done by key.
                // Please check index.js in OldExtractorProviders folder.

                console.log('Using Extractor: '.cyan + key.yellow);
                let extractedCards = this.providers[key].extract(aggregate[key]);
                //console.error(extractedCards);

                cards.push(...extractedCards);
            } catch (err) {
                console.log('ERROR IN AGGREGATE EXTRACTOR');
                console.error(err);
            }
        }


        return cards;
    }

}

module.exports = AggregateExtractor;