import {EngineElementInterface} from "./engine.element.interface";
import {NameNormalizer} from "../utils/name.normalizer";

import _ from 'lodash';

class EngineSeriePerson {
  [key: string]: number;
}

export class EngineSerie implements EngineElementInterface {

  public sources: Array<string>;
  public reliability: number;
  public booksName: Array<string>;
  public seriesName: string;
  private _authors: EngineSeriePerson;
  private _illustrators: EngineSeriePerson;
  private _editors: EngineSeriePerson;

  public get authors(): EngineSeriePerson {
    return this._authors;
  }

  public set authors(value: EngineSeriePerson) {
    this._authors = EngineSerie.normalizePersonObject(value);
  }

  get illustrators(): EngineSeriePerson {
    return this._illustrators;
  }

  set illustrators(value: EngineSeriePerson) {
    this._illustrators = EngineSerie.normalizePersonObject(value);
  }

  get editors(): EngineSeriePerson {
    return this._editors;
  }

  set editors(value: EngineSeriePerson) {
    this._editors = EngineSerie.normalizePersonObject(value);
  }

  static normalizePersonObject(value: EngineSeriePerson) {
    let keys = Object.keys(value),
      normalizedKeys = NameNormalizer.normalizeArray(keys),
      values = Object.values(value);

    return _.zipObject(normalizedKeys, values);
  }

  authorsArray() {
    return Object.keys(this._authors);
  }

  illustratorsArray() {
    return Object.keys(this._illustrators);
  }

  editorsArray() {
    return Object.keys(this._editors);
  }

}
