import {EngineElementDefault} from "./engine.element.default";
import faker from "faker";

const TYPES_DETAILS: {[key: string]: TypeDetail} = {
  person: {
    descriptors: ["author", "drawer"],
    faker: faker.name.findName
  },
  shortText: {
    descriptors: ["bookName", "seriesName", "type", "isbn", "seriesNo", "bookNumber",
      "numberOfPages", "editor", "seriesTotal", "jobs", "nationality"],
    faker: faker.commerce.productName
  },
  text: {
    descriptors: ["synopsis", "notablework", "biography"],
    faker: faker.lorem.paragraph
  },
  date: {
    descriptors: ["published", "edited", "birth", "death"],
    faker: faker.date.past
  },
  url: {
    descriptors: ["shop", "wikipedia"],
    faker: faker.internet.url
  },
  image: {
    descriptors: ["cover", "image"],
    faker: faker.image.image
  }
};

/**
 * Mocker which generate a random EngineElementDefault based on basic types.
 */
export class EngineElementMocker {

  public generate(): EngineElementDefault {

    let chosenType = chooseRandom(Object.keys(TYPES_DETAILS)),
        typeDetail = TYPES_DETAILS[chosenType],
        descriptor = chooseRandom(typeDetail.descriptors),
        value = typeDetail.faker();

    let element = new EngineElementDefault();
        Object.assign(element, {sources: [], reliability: 1, chosenType, descriptor, value});

    return element;
  }

}

interface TypeDetail {
  descriptors: string[],
  faker: Function
}

function chooseRandom(elements: any[]): any {
  let index = Math.floor(Math.random() * elements.length);

  return elements[index];
}
