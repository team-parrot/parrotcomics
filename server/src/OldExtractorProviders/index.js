module.exports = {
    books: require('./BooksExtractor'),
    persons: require('./PersonsExtractor'),
    series: require('./SeriesExtractor'),
    bag: require('./BagExtractor')
};