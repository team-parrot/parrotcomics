const axios = require('cachios');
require('colors');

const XPathTools = require('../XpathTools.js');
const Blocks = require('./Commons.js');

class CDiscountExtractor {

    static matches(url) {
        return (/cdiscount\.com\/livres-bd\//.test(url));
    }

    static async getNormalizedBlocks(url, sessionBag) {
        console.assert(CDiscountExtractor.matches(url));

        let normalized = Blocks.newNormalizedBlocks();
        let concurrentPromises = [];

        // One "concurrent promise" per block
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                // Here, every XPath query used for this block
                '//*[@id="fpZnPrdMain"]//div[@class="fpDesCol"]/h1/text()',
                '//span[contains(@class,"price")]/@content',

            ], url)
            .then(xpathResults => {
                const name = String(xpathResults[0]).trim();
                const price = Number(/\w+="([^"]+)"/.exec(String(xpathResults[1]))[1]);
                normalized.others.push({
                    sources: [url],
                    reliability: 1,
                    type: "price",
                    descriptor: String(xpathResults[0]).trim(),
                    value: price
                });
                // No return, just modify directly normalized
            })
            .catch(error => {
                console.log("Can't get price from CDiscount.com.".red);
                // console.error(error);
            }));

        await Promise.all(concurrentPromises);
        return normalized;
    }
}

module.exports = CDiscountExtractor;