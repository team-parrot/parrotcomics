import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {EngineBook} from "../../../services/engine/entities/engine.book";

@Component({
  selector: 'app-book.dialog',
  templateUrl: './book.dialog.component.html',
  styleUrls: ['./book.dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BookDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {element: EngineBook}) { }

  ngOnInit() {
  }

}
