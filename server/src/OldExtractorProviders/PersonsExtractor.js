const ExtractorInterface = require('./AbstractExtractor');

const CARD_MODELS = {
    name: {multiple: false, type: "person", descriptor: "bookName"},
    job: {multiple: false, type: "shortText", descriptor: "jobs"},
    bio: {multiple: false, type: "text", descriptor: "biography"},
    birthDate: {multiple: false, type: "date", descriptor: "birth"},
    deathDate: {multiple: false, type: "date", descriptor: "death"},
    notableWorks: {multiple: false, type: "text", descriptor: "notableWorks"},
    nationality: {multiple: false, type: "shortText", descriptor: "nationality"},
    urlImg: {multiple: false, type: "image", descriptor: "image"},
};

class PersonsExtractor extends ExtractorInterface {

    constructor() {
        super(CARD_MODELS);
    }

}

module.exports = PersonsExtractor;