
import {MAT_DIALOG_DATA} from "@angular/material";
import {Component, Inject} from "@angular/core";

@Component({
  selector: 'links-card-dialog',
  template: `
    <h2 mat-dialog-title>Liens associés</h2>
    <mat-dialog-content>
      <ul>
        <li *ngFor="let link of data.links"><a target="_blank" [href]="link">{{link}}</a></li>
      </ul>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button mat-dialog-close><mat-icon>close</mat-icon> Fermer</button>
    </mat-dialog-actions>
  `
})
export class LinksDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
