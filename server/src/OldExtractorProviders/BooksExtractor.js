const AbstractExtractor = require('./AbstractExtractor');
const Card = require('../Card');

const CARD_MODELS = {
    bookName: {multiple: false, type: "shortText", descriptor: "bookName"},
    authors: {multiple: true, type: "person", descriptor: "author"},
    illustrators: {multiple: true, type: "person", descriptor: "drawer"},
    seriesName: {multiple: false, type: "shortText", descriptor: "seriesName"},
    seriesNo: {multiple: false, type: "shortText", descriptor: "seriesNo"},
    seriesTotal: {multiple: false, type: "shortText", descriptor: "seriesTotal"},
    numberOfPages: {multiple: false, type: "shortText", descriptor: "numberOfPages"},
    bookNumber: {multiple: false, type: "shortText", descriptor: "bookNumer"},
    isbn: {multiple: false, type: "shortText", descriptor: "isbn"},
    synopsis: {multiple: false, type: "text", descriptor: "synopsis"},
    editors: {multiple: true, type: "shortText", descriptor: "editor"},
    publishedDate: {multiple: false, type: "date", descriptor: "published"},
    urlCover: {multiple: false, type: "image", descriptor: "cover"},
};

class BooksExtractor extends AbstractExtractor {

    constructor() {
        super(CARD_MODELS);
    }

}

module.exports = BooksExtractor;