export interface EngineServiceInterface {

  search(query: string, followSession: boolean): Promise<any>;

}
