const axios = require('cachios');
require('colors');

const XPathTools = require('../XpathTools.js');
const Blocks = require('./Commons.js');

class FnacExtractor {

    static matches(url) {
        return (/livre\.fnac\.com\//.test(url));
    }

    static async getNormalizedBlocks(url, sessionBag) {
        console.assert(FnacExtractor.matches(url));

        let normalized = Blocks.newNormalizedBlocks();
        let concurrentPromises = [];

        // One "concurrent promise" per block
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                // Here, every XPath query used for this block
                '//h1[@class="f-productHeader-Title"]/text()',
                '//div[@class="f-priceBox"]/span[contains(@class, "f-priceBox-price")]/text()',
                '//div[@class="f-priceBox"]/span[contains(@class, "f-priceBox-price")]/sup/text()',
            ], url)
            .then(xpathResults => {
                let p1 = Number(xpathResults[1]);
                let p2 = Number(/(\d+)/.exec(xpathResults[2])[1]);
                let price = p1 + p2 / 100;
                normalized.others.push({
                    sources: [url],
                    reliability: 1,
                    type: "price",
                    descriptor: String(xpathResults[0]).trim(),
                    value: price
                });
                // No return, just modify directly normalized
            })
            .catch(error => {
                console.log("Can't get price from Fnac.com.".red);
                // console.error(error);
            }));

        await Promise.all(concurrentPromises);
        return normalized;
    }
}

module.exports = FnacExtractor;