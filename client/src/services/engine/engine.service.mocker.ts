import {Injectable} from "@angular/core";
import {EngineElementMocker} from "./entities/engine.element.mocker";
import {EngineElementDefault} from "./entities/engine.element.default";
import {EngineServiceInterface} from "./engine.service.interface";

/**
 * Change this constant to alter mocker latency
 * @type {number}
 */
const FAKE_LATENCY_MS = 500;
const RESULT_LIMIT = 30;

/**
 * A simple mock of the search engine.
 * @Deprecated
 */
@Injectable()
export class EngineMocker implements EngineServiceInterface {

  constructor(private elementMocker: EngineElementMocker) {}

  public search(query: string, followSession: boolean): Promise<Array<EngineElementDefault>> {
    return new Promise((resolve) => {
      setTimeout(() => resolve(this.generateElementArray(RESULT_LIMIT)), FAKE_LATENCY_MS)
    });
  }

  private generateElementArray(limit: number) : Array<EngineElementDefault> {
    let elements: Array<EngineElementDefault> = [];

    for (let i=0; i<limit; i++) {
      elements.push(this.elementMocker.generate());
    }

    return elements;
  }

}
