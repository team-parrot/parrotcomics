/**
 * A simple normalizer for names. In fact the search engine often returns [lastname], [firstname]...
 * But for the customer, it's a really bad presentation (in my opinion).
 * So we transform the string to have [firstname] [lastname]
 */
export class NameNormalizer {

  static normalize(value: string) {
    let commaIndex = value.indexOf(',');

    if (-1 !== commaIndex) {
      let start = value.slice(0, commaIndex),
        end = value.slice(commaIndex+1, value.length);

      return (end+' '+start).trim();
    }

    return value;
  }

  static normalizeArray(values: Array<string>) {
    let normalized = [];

    for (let value of values) {
      normalized.push(NameNormalizer.normalize(value));
    }

    return normalized;
  }

}
