import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {EngineSerie} from "../../../../services/engine/entities/engine.serie";

@Component({
  selector: 'card-seriecontent',
  templateUrl: './seriecontent.component.html',
  styleUrls: ['./seriecontent.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SerieContentComponent implements OnInit {

  @Input() element: EngineSerie;

  constructor() { }

  ngOnInit() {
  }

  authorsArray() {
    return Object.keys(this.element.authors);
  }

  illustratorsArray() {
    return Object.keys(this.element.illustrators);
  }

  editorsArray() {
    return Object.keys(this.element.editors);
  }

}
