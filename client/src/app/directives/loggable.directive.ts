import {Directive, Input} from "@angular/core";

@Directive({
  selector: '[loggable]'
})
export class LoggableDirective {

  @Input()
  set logText(value) {
    console.debug(value);
  }

}
