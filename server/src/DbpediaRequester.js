const axios = require('axios');

const SPARQL_DEFAULT_PARAMETERS = {
    "default-graph-uri": "http://fr.dbpedia.org",
    format: "application/json",
    timeout: 30000,
    debug: "on"
};

const DEFAULT_CACHE_TIMEOUT = 120;

const REQUEST_STATUS_OK = 200;

const DEFAULT_ENDPOINT_URI = "http://fr.dbpedia.org/sparql";

/**
 * A simple AJAX requester for DBpedia. Uses axios and promises for an easier usage.
 */
class DbpediaRequester {

    constructor(endpoint = DEFAULT_ENDPOINT_URI, defaultParameters = SPARQL_DEFAULT_PARAMETERS, cacheTimeout = DEFAULT_CACHE_TIMEOUT) {
        this.endpoint = endpoint;
        this.defaultParameters = defaultParameters;
        this.cache = {};
        this.cacheTimeout = cacheTimeout;
    }

    /**
     * Handle the query, and eventually store the result in cache.
     * @param query
     * @returns {Promise}
     */
    query(query) {
        console.log('Preparing query to DBPedia: ' + query);
        query = DbpediaRequester.sanitizeQuery(query);

        let {uri, params} = this.prepareRequestQuery(query);

        // If you want to know how promises work, go to https://scotch.io/tutorials/javascript-promises-for-dummies
        // (Don't worry about the link bro, we have the right to discover things in JS...)
        return new Promise((resolve, reject) => {
            if (this.queryInCache(query)) {
                console.log('The query is not stored in cache. Calling dbPedia...');
                axios
                    .get(uri, {params})
                    .then((response) => {
                        console.log('Response got from DBPedia:');
                        console.error(response.status);
                        if (REQUEST_STATUS_OK === response.status) {
                            this.addQueryToCache(query, response.data);
                            resolve(response.data);
                        }
                    })
                    .catch((err) => { // Called even if something happens in "then" callback
                        console.log('Unable to recover details from DBPedia.');
                        reject(err);
                    })
            } else {
                console.log('Query resolved in cache.');
               resolve(this.cache[query].data);
            }
        });
    }

    /**
     * Prepare the input query for db search.
     * @param query
     * @returns {String}
     */
    static sanitizeQuery(query) {
        return query;
    }

    /**
     *
     * @param query
     * @return {*} {uri: string, params: {*}}}
     */
    prepareRequestQuery(query) {
        return {
            uri: this.endpoint,
            params: {
                ...this.defaultParameters,
                query
            }
        }
    }

    addQueryToCache(query, data) {
        this.cache[query] = {data, datetime: new Date};
    }

    /**
     * @param query {String}
     * @returns {boolean}
     */
    queryInCache(query) {
        return 'undefined' === typeof this.cache[query] || +(new Date) - 1000*this.cacheTimeout > this.cache[query].datetime;
    }

}

module.exports = DbpediaRequester;