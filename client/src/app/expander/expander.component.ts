import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-expander',
  templateUrl: './expander.component.html',
  styleUrls: ['./expander.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ExpanderComponent implements OnInit {

  @Input() array: Array<string>;

  @Input() limit: number;

  @Input() unordered: boolean = true;

  private truncatedArray: Array<string>;
  private leftCount: number;

  constructor() { }

  ngOnInit() {
    this.truncatedArray = this.array.slice(0, this.limit);
    this.leftCount = this.array.length - this.truncatedArray.length;
  }

}
