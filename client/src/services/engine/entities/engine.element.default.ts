/**
 * An element represents the data used in search results.
 */
import {EngineElementInterface} from "./engine.element.interface";

export class EngineElementDefault implements EngineElementInterface{

  public sources: string[];
  public reliability: number;
  public type: string;
  public descriptor: string;
  public value: any;

}
