const expressFactory = require('express');
const DbpediaRequester = require('./DbpediaRequester');
const SearchEngine = require('./SearchEngine.js');
require('colors');

// The main app class.
class App {

    constructor(port = 3000) {
        this.port = port;
        this.express = expressFactory();
        this.dbpediaRequester = new DbpediaRequester();
        this.searchEngine = new SearchEngine();
    }

    init() {
        // Search access point. Simple, yolo.
        this.express.get('/search', (req, res) => {
            res.header('Access-Control-Allow-Origin', '*');

            this.searchEngine.handle(req.query)
                .then((data) => {
                    res.json({success: true, ...data});
                    console.log('Success, here are the blocks!'.rainbow);
                })
                .catch((error) => {
                    console.log("Can't get blocks:".red);
                    console.error(error);
                    if ('string' === typeof error) {
                        res.json({success: false, error: error});
                    } else {
                        res.json({success: false, error: "Can't get blocks."});
                    }
                });
        });

        // Sparql access point. Simple, yolo.
        this.express.get('/sparql', (req, res) => {
            // req.query is the querystring map.
            // res.json output the object in plain JSON.
            if ('undefined' === typeof req.query.query) {
                res.json({success: false, error: 'Empty query.'});
                return;
            }

            // Required parameters : query|session_id, limit, offset

            this.dbpediaRequester.query(req.query.query)
                .then((data) => {
                    res.json({success: true, data});
                })
                .catch((error) => {
                    console.error(error);
                    res.json({success: false, error: error.data});
                });
        });

        this.express.use(expressFactory.static(__dirname + '/../public'));

        // Let's listen to the given port.
        this.express.listen(this.port, () => {
            console.log(`Server started on port ${this.port}`);
        });
    }

}

module.exports = App;
