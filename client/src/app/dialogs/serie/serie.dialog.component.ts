import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {EngineSerie} from "../../../services/engine/entities/engine.serie";

@Component({
  selector: 'app-serie-dialog',
  templateUrl: './serie.dialog.component.html',
  styleUrls: ['./serie.dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SerieDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {element: EngineSerie}) { }

  ngOnInit() {
  }

}
