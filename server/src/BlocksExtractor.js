const axios = require('cachios');
const _ = require('lodash');
require('colors');

const EXTRACTORS = require('./BlocksExtractors');

class BlocksExtractor {

    /**
     * Extracts blocks from a given URL, into the normalized form.
     * @param url
     * @param sessionBag: bag into which URL are pushed for later extraction
     * @returns {Promise<[{series: Array, books: Array, persons: Array, others: Array}]>}
     */
    static async extractBlocks(url, sessionBag) {
        let normalizedBlocksPromiseList = [];
        for (let extractor of EXTRACTORS) {
            if (extractor.matches(url)) {
                normalizedBlocksPromiseList.push(extractor
                    .getNormalizedBlocks(url, sessionBag)
                    .catch((error) => {
                        console.log('Error when getting normalized blocks: '.red + String(error).red);
                        if (!/[4-5]0\d/.test(String(error))) {
                            console.error(error);
                        }
                        return {};
                    }));
            }
        }
        return (await Promise.all(normalizedBlocksPromiseList)).filter(BlocksExtractor.nonEmpty);
    }

    static nonEmpty(blocksObj) {
        if (blocksObj === undefined || !blocksObj) return false;
        for (let attr in blocksObj) if (blocksObj.hasOwnProperty(attr))
            if (!_.isEmpty(blocksObj[attr])) return true;
        return false;
    }
}

module.exports = BlocksExtractor;
