import {EngineElementInterface} from "./engine.element.interface";
import {NameNormalizer} from "../utils/name.normalizer";

export class EnginePerson implements EngineElementInterface {

  sources: Array<string>;
  reliability: number;
  type: string;
  bio: string;
  birthDate: string;
  deathDate: string;
  notableWorks: string;
  nationality: string;
  urlImg: string;
  private _name: string;

  public get name(): string {
    return this._name;
  }

  public set name(value: string) {
    this._name = NameNormalizer.normalize(value);
  }

}
