export interface EngineElementInterface {

  sources: Array<string>;
  reliability: number;

}
