let microdata = require('microdata-node');
const axios = require('cachios');
require('colors');

const XPathTools = require('../XpathTools.js');
const Blocks = require('./Commons.js');

class BedethequeAuthorsExtractor {

    static matches(url) {
        return (/bedetheque.com\/auteur-/i.test(url));
    }

    static async getNormalizedBlocks(url, sessionBag) {
        console.assert(BedethequeAuthorsExtractor.matches(url));

        let normalized = Blocks.newNormalizedBlocks();
        let author = Blocks.newEmptyPerson(url, 10);
        author.type = "author";
        let concurrentPromises = [];

        // Name
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                '//div[@class="auteur-nom"]/h2/text()'
            ], url)
            .then(xpathResults => {
                author.name = String(xpathResults[0]);
            })
            .catch(error => {
                console.log("Can't get author name from bedetheque.com.".red);
                // console.error(error);
            }));

        // Visit card
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                '//ul[@class="auteur-info"]//label[text()="Nom :"]/../span/text()',
                '//ul[@class="auteur-info"]//label[text()="Prénom :"]/../span/text()',
                '//ul[@class="auteur-info"]//label[text()="Naissance :"]/../text()',
                '//ul[@class="auteur-info"]//label[text()="Décès :"]/../text()',
            ], url)
            .then(xpathResults => {
                // const bdr = /le (\d+)\/(\d+)\/(\d+)/.exec(xpathResults[2]);
                // const birthDate = new Date(Number(bdr[3]), Number(bdr[2]), Number(bdr[1]));
                // const ddr = /le (\d+)\/(\d+)\/(\d+)/.exec(xpathResults[3]);
                // const deathDate = new Date(Number(ddr[3]), Number(ddr[2]), Number(ddr[1]));
                // author.birthDate = birthDate;
                // author.deathDate = deathDate;
                author.birthDate = String(xpathResults[2]);
                author.deathDate = String(xpathResults[3]);
            })
            .catch(error => {
                console.log("Can't get visit card information from bedetheque.com.".red);
                console.error(error);
            }));

        // Biography and picture
        concurrentPromises.push(XPathTools.getMultipleXpathResultFromURL(
            [
                '//p[@class="bio"]/text()',
                '//div[@class="auteur-image"]//img/@src',
            ], url)
            .then(xpathResults => {
                author.bio = String(xpathResults[0]);
                author.urlImg = /src="([^"]*)"/.exec(String(xpathResults[1]))[1];
            })
            .catch(error => {
                console.log("Can't get biography / picture information from bedetheque.com.".red);
                console.error(error);
            }));

        await Promise.all(concurrentPromises);
        normalized.persons.push(author);
        return normalized;
    }
}

module.exports = BedethequeAuthorsExtractor;
