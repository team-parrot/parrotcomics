import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material";

import {ParrotService} from "../../services/parrot.service";
import {EngineService} from "../../services/engine/engine.service";
import {EngineElementDefault} from "../../services/engine/entities/engine.element.default";

const DEFAULT_ENGINE_ENDPOINT = "http://localhost:3000/search";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  private parrot: String;
  private showSearchResults: Boolean = false;
  private searching: Boolean = false;
  private endOfResults: Boolean = false;
  private cardsLimit: number = 20;

  private cardBlocks: Array<Array<EngineElementDefault>> = [];

  private form = new FormGroup({ query: new FormControl('', [Validators.required, Validators.minLength(2)]) });

  constructor(private parrotService: ParrotService, private engineService: EngineService, private snackBar: MatSnackBar) {
    this.setRandomParrot();

    this.engineService.endpoint = DEFAULT_ENGINE_ENDPOINT;
  }

  public setRandomParrot() :void {
    this.parrot = this.parrotService.getParrot();
  }

  public search(): void {
    this.showSearchResults = true;
    this.searching = true;
    this.endOfResults = false;

    this.cardBlocks = [];
    this.getResults();

  }

  public nextResults(force: boolean = false): void {
    if (true === force || (false === this.searching && false === this.endOfResults)) {
      this.searching = true;
      this.getResults(true);
    }
  }

  private getResults(wantNext: boolean = false): void {
    this.engineService.search(this.form.get('query').value, wantNext)
      .then((elements: Array<EngineElementDefault>) => {
        if (elements.length === 0) {
          this.searching = false;
          this.endOfResults = true;
          this.snackBar.open('La fin de recherche a été atteinte.', 'Ok !', {
            duration: 10000
          });

          return true;
        }

        console.debug(elements);
        this.cardBlocks.push(elements);

        this.searching = false;
      })
      .catch((err) => {
        console.error(err);
        this.snackBar.open(err, undefined, {
          duration: 10000
        });
      })
  }

  public removeCard(cardData: {id: number, page: number}) {
    console.log('event received: remove card '+cardData.id+' '+cardData.page);

    if (cardData.page < this.cardBlocks.length) {
      this.cardBlocks[cardData.page].splice(cardData.id, 1);
    }

    console.debug(this.cardBlocks);
  }

  ngOnInit() {
  }

}
