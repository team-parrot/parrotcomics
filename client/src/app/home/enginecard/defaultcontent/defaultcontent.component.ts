import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {EngineElementDefault} from "../../../../services/engine/entities/engine.element.default";
import {MatDialog} from "@angular/material";
import {LinksDialogComponent} from "../../../links.dialog.component";

const TYPES_TITLEABLE   = ["person", "shortText", "date"];
const TYPES_LINKABLE    = ['url'];
const TYPES_IMAGEABLE   = ["image"];
const TYPES_CONTENTABLE = ["text", "person"];

const LOCALE_DESCRIPTOR = {
  author: "Auteur",
  drawer: "Dessinateur",
  editor: "Éditeur",
  name: "Nom de l'oeuvre",
  type: "Type",
  isbn: "ISBN",
  synopsis: "Synopsis",
  notablework: "Faits notoires",
  published: "Date de publication",
  edited: "Date d'édition",
  birth: "Date de naissance",
  death: "Date de décès",
  shop: "Boutique en ligne",
  wikipedia: "Wikipedia",
  cover: "Couverture",
  image: "Image",
};

const VALUE_TYPE_PREFIX = {
  price: ''
};

const VALUE_TYPE_SUFFIX = {
  price: ' €'
};

@Component({
  selector: 'card-defaultcontent',
  templateUrl: './defaultcontent.component.html',
  styleUrls: ['./defaultcontent.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DefaultContentComponent implements OnInit {

  @Input() element: EngineElementDefault;

  constructor() {}

  formattedValue() {
    let string = '';

    if (VALUE_TYPE_PREFIX[this.element.type] !== undefined) {
      string += VALUE_TYPE_PREFIX[this.element.type];
    }

    string += this.element.value;

    if (VALUE_TYPE_SUFFIX[this.element.type] !== undefined) {
      string += VALUE_TYPE_SUFFIX[this.element.type];
    }

    return string;
  }

  ngOnInit() {
  }

}
