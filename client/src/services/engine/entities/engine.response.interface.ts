import {EngineSerie} from "./engine.serie";
import {EngineBook} from "./engine.book";
import {EngineElementDefault} from "./engine.element.default";
import {EnginePerson} from "./engine.person";

export interface EngineResponse {
  success: boolean;
  session_id: string;
  blocks?: {
    series: Array<EngineSerie>,
    books: Array<EngineBook>,
    persons: Array<EnginePerson>,
    others: Array<EngineElementDefault>
  } | null;
  error?: any;
}
